#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"


#define HEAP_SIZE 4096

extern void debug(const char *fmt, ...);

static void *map_pages(void const *addr, size_t length, int additional_flags);


void initialize_and_test(void (*test_func)()) {
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);

    test_func();

    heap_term();
}

void first_test() {
    void *allocated = _malloc(2);
    assert(allocated != NULL);

    _free(allocated);
}

void second_test() {
    void *allocated_first = _malloc(64);
    assert(allocated_first);

    void *allocated_second = _malloc(132);
    assert(allocated_second);

    void *allocated_third = _malloc(30);
    assert(allocated_third);

    _free(allocated_second);

    debug("Heap: ");
    debug_heap(stdout, HEAP_START);

    _free(allocated_first);
    _free(allocated_third);
}

void third_test() {
    void *allocated_first = _malloc(64);
    void *allocated_second = _malloc(128);
    void *allocated_third = _malloc(132);
    assert(allocated_first);
    assert(allocated_second);
    assert(allocated_third);

    debug("Delete 2 blocks\n");
    _free(allocated_third);
    _free(allocated_first);
    assert(allocated_second != NULL);

    debug("Heap: \n");
    debug_heap(stdout, HEAP_START);

    _free(allocated_second);
}

void forth_test() {
    debug("Create region\n");
    void *mmap_addr = map_pages(HEAP_START, 15, MAP_FIXED);
    assert(mmap_addr);

    debug("Alloc to occupied region\n");
    void *alloc_first = _malloc(REGION_MIN_SIZE * 2);
    assert(alloc_first != MAP_FAILED);

    _free(alloc_first);
    debug(" passed\n");
}

int main() {
    initialize_and_test(first_test);
    initialize_and_test(second_test);
    initialize_and_test(third_test);
    initialize_and_test(forth_test);

    debug("All tests passed\n");

    return 0;
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}